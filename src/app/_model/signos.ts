import { Paciente } from "./paciente";

export class Signos {
  idSignos: number;
  paciente: Paciente;
  //fecha: Date;
  fecha: string;
  temperatura: string;
  pulso: string;
  ritmo: string;
}
