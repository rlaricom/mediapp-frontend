import { Component, OnInit } from '@angular/core';
import { JwtHelperService } from '@auth0/angular-jwt';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-perfil',
  templateUrl: './perfil.component.html',
  styleUrls: ['./perfil.component.css']
})
export class PerfilComponent implements OnInit {

  usuario: string;

  roles: string[];

  rolesLabel: string;

  constructor() { }

  ngOnInit(): void {
    this.rolesLabel = '';
    const helper = new JwtHelperService();
    let token = sessionStorage.getItem(environment.TOKEN_NAME);

    const decodedToken = helper.decodeToken(token);

    console.log(decodedToken);

    this.usuario = decodedToken.user_name;
    this.roles = decodedToken.authorities;

    this.roles.forEach(rol => {
      this.rolesLabel = this.rolesLabel + " " + rol + ","
    });

    // quitando la ultima coma
    this.rolesLabel=this.rolesLabel.substr(0,(this.rolesLabel.length-1))
  }

}
