import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { Paciente } from 'src/app/_model/paciente';
import { Signos } from 'src/app/_model/signos';
import { PacienteService } from 'src/app/_service/paciente.service';
import { SignosService } from 'src/app/_service/signos.service';
import { map } from 'rxjs/operators';
import * as moment from 'moment';
import { switchMap } from 'rxjs/operators';

@Component({
  selector: 'app-signos-edicion',
  templateUrl: './signos-edicion.component.html',
  styleUrls: ['./signos-edicion.component.css']
})
export class SignosEdicionComponent implements OnInit {

  id: number;
  signos: Signos;
  form: FormGroup;
  edicion: boolean = false;

  pacientes: Paciente[];

  // utilies para el autocomplete
  myControlPaciente: FormControl = new FormControl();

  pacientesFiltrados$: Observable<Paciente[]>;

  fechaSeleccionada: Date = new Date();

  pacienteSeleccionado: Paciente;

  constructor(
    private signosService: SignosService,
    private pacienteService : PacienteService,
    private route: ActivatedRoute,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.signos = new Signos();

    //['id', 'paciente', 'temperatura', 'pulso', 'ritmo','fecha', 'acciones'];

    this.form=new FormGroup({
      'id':new FormControl(0),
      'paciente': this.myControlPaciente,
      'temperatura':new FormControl(''),
      'pulso':new FormControl(''),
      'ritmo':new FormControl(''),
      'fecha': new FormControl(new Date()),
    });

    this.listarPacientes();

    this.pacientesFiltrados$ = this.myControlPaciente.valueChanges.pipe(map(val => this.filtrarPacientes(val)));

    this.route.params.subscribe((params: Params) => {
      this.id = params['id'];
      this.edicion = params['id'] != null;
      this.initForm();
    });
  }

  filtrarPacientes(val: any){
    if (val != null && val.idPaciente > 0) {
      return this.pacientes.filter(el =>
        el.nombres.toLowerCase().includes(val.nombres.toLowerCase()) || el.apellidos.toLowerCase().includes(val.apellidos.toLowerCase()) || el.dni.includes(val.dni)
      );
      //EMPTY de RxJS
    }
    return this.pacientes.filter(el =>
      el.nombres.toLowerCase().includes(val?.toLowerCase()) || el.apellidos.toLowerCase().includes(val?.toLowerCase()) || el.dni.includes(val)
    );
  }

  listarPacientes() {
    this.pacienteService.listar().subscribe(data => {
      this.pacientes = data;
    });
  }

  mostrarPaciente(val: Paciente) {
    return val ? `${val.nombres} ${val.apellidos}` : val;
  }

  initForm() {
    if (this.edicion) {
      this.signosService.listarPorId(this.id).subscribe(data => {
        console.log(data);
        let id = data.idSignos;
        //let paciente = data.paciente;

        this.myControlPaciente =  new FormControl(data.paciente);
        this.listarPacientes();
        this.pacientesFiltrados$ = this.myControlPaciente.valueChanges.pipe(map(val => this.filtrarPacientes(val)));
        let temperatura = data.temperatura;
        let pulso = data.pulso;
        let ritmo = data.ritmo;
        let fecha = data.fecha;
        this.form = new FormGroup({
          'id': new FormControl(id),
          'paciente': this.myControlPaciente,
          //'paciente': this.myControlPaciente,

          'temperatura':new FormControl(temperatura),
          'pulso':new FormControl(pulso),
          'ritmo':new FormControl(ritmo),
          'fecha': new FormControl(fecha),
        });
      });
    }
  }

  operar() {
    this.signos.idSignos = this.form.value['id'];
    this.signos.paciente = this.form.value['paciente'];
    this.signos.temperatura = this.form.value['temperatura'];
    this.signos.pulso = this.form.value['pulso'];
    this.signos.ritmo = this.form.value['ritmo'];
    this.signos.fecha = moment(this.form.value['fecha']).format('YYYY-MM-DDTHH:mm:ss');

    if (this.signos != null && this.signos.idSignos > 0) {
      //BUENA PRACTICA
      this.signosService.modificar(this.signos).pipe(switchMap(() => {
        return this.signosService.listar();
      })).subscribe(data => {
        this.signosService.setSignosCambio(data);
        this.signosService.setMensajeCambio("Se modificó");
      });

    } else {
      //PRACTICA COMUN
      this.signosService.registrar(this.signos).subscribe(data => {
        this.signosService.listar().subscribe(signo => {
          this.signosService.setSignosCambio(signo);
          this.signosService.setMensajeCambio("Se registró");
        });
      });
    }

    this.router.navigate(['pages/signos']);
  }

}
